<?php
declare(strict_types=1);

namespace Tests\App\Functional\Http\Controllers\MailChimp;

use Tests\App\TestCases\MailChimp\MemberTestCase;

class MembersControllerTest extends MemberTestCase
{
    /**
     * Test application creates successfully member and returns it back with id from MailChimp.
     *
     * @return void
     */
    public function testCreateMemberSuccessfully(): void
    {
        // create list
        $this->post('/mailchimp/lists', static::$listData);
        $list = \json_decode($this->response->content(), true);
    
        // create the member for the list
        $this->post(\sprintf('/mailchimp/lists/%s/members', $list['list_id']), static::$memberData );

        $content = \json_decode($this->response->getContent(), true);

        $this->assertResponseOk();
        $this->seeJson(static::$memberData);
        self::assertArrayHasKey('mail_chimp_id', $content);
        self::assertNotNull($content['mail_chimp_id']);
    }

    /**
     * Test application returns error response with errors when member validation fails.
     *
     * @return void
     */
    public function testCreateMemberValidationFailed(): void
    {
         // create list
         $this->post('/mailchimp/lists', static::$listData);
         $list = \json_decode($this->response->content(), true);
 
         // create the member for the list
         $this->post(\sprintf('/mailchimp/lists/%s/members', $list['list_id']) );

        $content = \json_decode($this->response->getContent(), true);

        $this->assertResponseStatus(400);
        self::assertArrayHasKey('message', $content);
        self::assertArrayHasKey('errors', $content);
        self::assertEquals('Invalid data given', $content['message']);

        foreach (static::$memberData as $key => $value) {
            if (\in_array($key, static::$notRequired, true)) {
                continue;
            }

            self::assertArrayHasKey($key, $content['errors']);
        }
    }

    /**
     * Test application returns error response when member not found.
     *
     * @return void
     */
    public function testRemoveMemberNotFoundException(): void
    {
         // create list
         $this->post('/mailchimp/lists', static::$listData);
         $list = \json_decode($this->response->content(), true);
        
        // delete the non existing member for the list
        $this->delete(\sprintf('/mailchimp/lists/%s/members/invalid-member-id', $list['list_id']) );
        $this->assertMemberNotFoundResponse('invalid-member-id');
    }

    /**
     * Test application returns empty successful response when removing existing member.
     *
     * @return void
     */
    public function testRemoveMemberSuccessfully(): void
    {
         // create list
         $this->post('/mailchimp/lists', static::$listData);
         $list = \json_decode($this->response->content(), true);
 
         // create the member for the list
         $this->post(\sprintf('/mailchimp/lists/%s/members', $list['list_id']), static::$memberData );

        $member = \json_decode($this->response->content(), true);

        $this->delete(\sprintf('/mailchimp/lists/%s/members/%s', $list['list_id'], $member['member_id']));

        $this->assertResponseOk();
        self::assertEmpty(\json_decode($this->response->content(), true));
    }

    /**
     * Test application returns error response when member not found.
     *
     * @return void
     */
    public function testShowMemberNotFoundException(): void
    {
        // create list
        $this->post('/mailchimp/lists', static::$listData);
        $list = \json_decode($this->response->content(), true);

        // create the member for the list
        $this->post(\sprintf('/mailchimp/lists/%s/members', $list['list_id']), static::$memberData );

        $this->get(\sprintf('/mailchimp/lists/%s/members/invalid-member-id', $list['list_id']));

        $this->assertMemberNotFoundResponse('invalid-member-id');
    }

    /**
     * Test application returns successful response with member data when requesting existing member.
     *
     * @return void
     */
    public function testShowMemberSuccessfully(): void
    {
        // create list
        $list = $this->createList(static::$listData);

        $member = $this->createMember(static::$memberData, $list->getId());
        $this->get(\sprintf('/mailchimp/lists/%s/members/%s', $list->getId(), $member->getId()));
        $content = \json_decode($this->response->content(), true);

        $this->assertResponseOk();

        foreach (static::$memberData as $key => $value) {
            self::assertArrayHasKey($key, $content);
            self::assertEquals($value, $content[$key]);
        }
    }

    /**
     * Test application returns error response when member not found.
     *
     * @return void
     */
    public function testUpdateMemberNotFoundException(): void
    {
        // create list
        $list = $this->createList(static::$listData);

        $this->put(\sprintf('/mailchimp/lists/%s/members/invalid-member-id', $list->getId()) );

        $this->assertMemberNotFoundResponse('invalid-member-id');
    }

    /**
     * Test application returns successfully response when updating existing member with updated values.
     *
     * @return void
     */
    public function testUpdateMemberSuccessfully(): void
    {
        // create list
        $this->post('/mailchimp/lists', static::$listData);
        $list = \json_decode($this->response->content(), true);

        // create the member for the list
        $this->post(\sprintf('/mailchimp/lists/%s/members', $list['list_id']), static::$memberData );

       $member = \json_decode($this->response->content(), true);
       
        $this->put(\sprintf('/mailchimp/lists/%s/members/%s', $list['list_id'], $member['member_id']), ['language' => 'ta']);
        $content = \json_decode($this->response->content(), true);

        $this->assertResponseOk();

        foreach (static::$memberData as $key => $value) {
            self::assertArrayHasKey($key, $content);
            self::assertEquals('ta', $content['language']);
        }
    }

    /**
     * Test application returns error response with errors when member validation fails.
     *
     * @return void
     */
    public function testUpdateMemberValidationFailed(): void
    {
        $list = $this->createList(static::$listData);
        $member = $this->createMember(static::$memberData, $list->getId() );

        $this->put(\sprintf('/mailchimp/lists/%s/members/%s', $list->getId(), $member->getId()), ['status' => 'invalid']);
        $content = \json_decode($this->response->content(), true);

        $this->assertResponseStatus(400);
        self::assertArrayHasKey('message', $content);
        self::assertArrayHasKey('errors', $content);
        self::assertArrayHasKey('status', $content['errors']);
        self::assertEquals('Invalid data given', $content['message']);
    }
}
